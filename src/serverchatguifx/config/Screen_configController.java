package serverchatguifx.config;





/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.jfoenix.controls.JFXTextField;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import serverchatguifx.classes.Log;
import serverchatguifx.log.Screen_logController;





/**
 * FXML Controller class
 *
 * @author pedro
 */
public class Screen_configController extends Screen_logController implements Initializable {
    

    @FXML
    private JFXTextField txt_server_ip;
    @FXML
    private JFXTextField txt_server_port;
    
    
    
    
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            String ip = InetAddress.getLocalHost().getHostAddress();
            txt_server_ip.setText(ip);
            
        } catch (UnknownHostException ex) {
            Logger.getLogger(Screen_configController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    @FXML
    private void connectServer(ActionEvent event){
        Log log = new Log(getTxArea_log());
        log.getTxArea_log().setVisible(true);
        log.getTxArea_log().appendText("teste");
 
        
        
    }
    
}
