package serverchatguifx.log;





/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
/**
 * FXML Controller class
 *
 * @author pedro
 */
public class Screen_logController implements Initializable {
    
    @FXML
    private TextArea txArea_log;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
                
    }  

    /**
     * @return the txArea_log
     */
    public TextArea getTxArea_log() {
        return txArea_log;
    }
    
    
}
