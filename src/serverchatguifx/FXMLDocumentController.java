/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverchatguifx;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import eu.hansolo.tilesfx.Tile;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import serverchatguifx.about.Screen_aboutController;
/**
 *
 * @author pedro
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private AnchorPane screen_main;
    @FXML
    private JFXButton btn_dash;
    @FXML
    private JFXButton btn_log;
    @FXML
    private JFXButton btn_config;
    @FXML
    private JFXButton bnt_about;
    @FXML
    private Pane pane_handle_screen;
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    void handleConfigScreen(ActionEvent event) throws IOException {
       pane_handle_screen.getChildren().clear();
       Pane pane = FXMLLoader.load(getClass().getResource("/serverchatguifx/config/Screen_config.fxml"));
       pane_handle_screen.getChildren().setAll(pane);

    }

    @FXML
    void handleDashboardScreen(ActionEvent event) throws IOException {
        Pane pane = FXMLLoader.load(getClass().getResource("/serverchatguifx/dash/screen_dashboard.fxml"));
        pane_handle_screen.getChildren().setAll(pane);

    }

    @FXML 
    void handleLogScreen(ActionEvent event) throws IOException {
        pane_handle_screen.getChildren().clear();
        Pane pane = FXMLLoader.load(getClass().getResource("/serverchatguifx/log/Screen_log.fxml"));
        pane_handle_screen.getChildren().setAll(pane);
    }

    @FXML
    void handleSobreScreen(ActionEvent event) throws IOException {
        pane_handle_screen.getChildren().clear();
        Pane pane = FXMLLoader.load(getClass().getResource("/serverchatguifx/about/Screen_about.fxml"));
        pane_handle_screen.getChildren().setAll(pane);
    }    
    
}
